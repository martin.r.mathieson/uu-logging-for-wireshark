/* rlc_lte_logger.c
 *
 * Example code for sending RLC LTE frames over UDP
 * Written by Pascal Quantin and Martin Mathieson
 * This header file may also be distributed under
 * the terms of the BSD Licence as follows:
 * 
 * Copyright (C) 2009 Pascal Quantin & Martin Mathieson. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE
 */



#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

typedef unsigned char  guint8;
typedef unsigned short guint16;
typedef unsigned int   guint32;
typedef unsigned long  guint64;
typedef int            gboolean;

typedef struct {
        time_t  secs;
        int     nsecs;
} nstime_t;

/* Forward-declare types */
struct packet_info;
typedef struct packet_info packet_info;
struct tvbuff_t;
typedef struct tvbuff_t tvbuff_t;

#include "../wireshark/epan/dissectors/packet-rlc-lte.h"


/* Globals where each frame is composed before sending */
static unsigned char g_PDUBuffer[16000];
static unsigned int  g_PDUOffset;
static unsigned char g_frameBuffer[16000];
static unsigned int  g_frameOffset;

/* UDP socket used for sending frames */
static int                  g_sockfd;

/* Remote serveraddress (where Wireshark is running) */
static struct sockaddr_in   g_serv_addr;


/* Write a RLC TM PDU */
static void EncodeDummyRLCPDU1(void)
{
    g_PDUOffset = 0;

    g_PDUBuffer[g_PDUOffset++] = 0x60;
    g_PDUBuffer[g_PDUOffset++] = 0x12;
    g_PDUBuffer[g_PDUOffset++] = 0x9b;
    g_PDUBuffer[g_PDUOffset++] = 0x3e;
    g_PDUBuffer[g_PDUOffset++] = 0x9c;
    g_PDUBuffer[g_PDUOffset++] = 0xc0;
    g_PDUBuffer[g_PDUOffset++] = 0x7f;
    g_PDUBuffer[g_PDUOffset++] = 0xf0;
    g_PDUBuffer[g_PDUOffset++] = 0x96;
    g_PDUBuffer[g_PDUOffset++] = 0x64;
    g_PDUBuffer[g_PDUOffset++] = 0x30;
    g_PDUBuffer[g_PDUOffset++] = 0x64;
    g_PDUBuffer[g_PDUOffset++] = 0xcb;
    g_PDUBuffer[g_PDUOffset++] = 0x05;
    g_PDUBuffer[g_PDUOffset++] = 0x23;
    g_PDUBuffer[g_PDUOffset++] = 0xc0;
}

/* Write a RLC AM control PDU */
static void EncodeDummyRLCPDU2(void)
{
    g_PDUOffset = 0;

    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x04;
}

/* Write a RLC AM data PDU */
static void EncodeDummyRLCPDU3(void)
{
    g_PDUOffset = 0;

    g_PDUBuffer[g_PDUOffset++] = 0x88;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x80;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x01;
    g_PDUBuffer[g_PDUOffset++] = 0x08;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x06;
    g_PDUBuffer[g_PDUOffset++] = 0x04;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x01;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0xFF;
    g_PDUBuffer[g_PDUOffset++] = 0x1D;
    g_PDUBuffer[g_PDUOffset++] = 0xA1;
    g_PDUBuffer[g_PDUOffset++] = 0x3D;
    g_PDUBuffer[g_PDUOffset++] = 0x28;
    g_PDUBuffer[g_PDUOffset++] = 0xC0;
    g_PDUBuffer[g_PDUOffset++] = 0xA8;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x01;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
}




/*******************************************/
/* Add framing header to RLC PDU and send. */
void SendFrame(guint8 rlcMode, guint8 direction, guint8 priority,
               guint16 ueid, guint16 channelType, guint16 channelId,
               guint8 UMSequenceNumberLength)
{
    ssize_t bytesSent;
    g_frameOffset = 0;
    unsigned short tmp16;

    /********************************************************************/
    /* Fixed start to each frame (allowing heuristic dissector to work) */
    /* Not NULL terminated */
    memcpy(g_frameBuffer+g_frameOffset, RLC_LTE_START_STRING,
           strlen(RLC_LTE_START_STRING));
    g_frameOffset += strlen(RLC_LTE_START_STRING);

    /******************************************************************************/
    /* Now write out fixed field (the mandatory element of struct rlc_lte_info)   */
    g_frameBuffer[g_frameOffset++] = rlcMode;

    /*************************************/
    /* Now conditional fields            */
    
    /* UM SN length */
    if (rlcMode == RLC_UM_MODE) {
        g_frameBuffer[g_frameOffset++] = RLC_LTE_SN_LENGTH_TAG;
        g_frameBuffer[g_frameOffset++] = UMSequenceNumberLength;
    }

    /*************************************/
    /* Now optional fields               */

    /* Direction */
    g_frameBuffer[g_frameOffset++] = RLC_LTE_DIRECTION_TAG;
    g_frameBuffer[g_frameOffset++] = direction;

    /* Priority */
    g_frameBuffer[g_frameOffset++] = RLC_LTE_PRIORITY_TAG;
    g_frameBuffer[g_frameOffset++] = priority;

    /* UEId */
    g_frameBuffer[g_frameOffset++] = RLC_LTE_UEID_TAG;
    tmp16 = htons(ueid);
    memcpy(g_frameBuffer+g_frameOffset, &tmp16, 2);
    g_frameOffset += 2;

    /* Channel Type */
    g_frameBuffer[g_frameOffset++] = RLC_LTE_CHANNEL_TYPE_TAG;
    tmp16 = htons(channelType);
    memcpy(g_frameBuffer+g_frameOffset, &tmp16, 2);
    g_frameOffset += 2;

    /* Channel Id */
    g_frameBuffer[g_frameOffset++] = RLC_LTE_CHANNEL_ID_TAG;
    tmp16 = htons(channelId);
    memcpy(g_frameBuffer+g_frameOffset, &tmp16, 2);
    g_frameOffset += 2;

    /***************************************/
    /* Now write the RLC PDU               */
    g_frameBuffer[g_frameOffset++] = RLC_LTE_PAYLOAD_TAG;

    /* Append actual PDU  */
    memcpy(g_frameBuffer+g_frameOffset, g_PDUBuffer, g_PDUOffset);
    g_frameOffset += g_PDUOffset;

    /* Send out the data over the UDP socket */
    bytesSent = sendto(g_sockfd, g_frameBuffer, g_frameOffset, 0,
                      (const struct sockaddr*)&g_serv_addr, sizeof(g_serv_addr));
    if (bytesSent != g_frameOffset) {
        fprintf(stderr, "sendto() failed - expected %u bytes, got %lu (errno=%d)\n",
                g_frameOffset, bytesSent, errno);
        exit(1);
    }
}


/*************************************************************************/
/* Main function                                                         */
/*  - set up socket + aserver address                                    */
/*  - encode and send some example LTE RLC frames using framing protocol */
int main(int argc, char *argv[])
{
    struct hostent *hp;

    if (argc < 3) {
        fprintf(stderr, "Usage: coclient <server-host> <server-port>\n");
        exit(1);
    }

    /***********************************/
    /* Create local socket             */
    g_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (g_sockfd == -1) {
        fprintf(stderr, "Error trying to create socket (errno=%d)\n", errno);
        exit(1);
    }

    /***************************************************/
    /* Get remote IP address from 1st command-line arg */
    g_serv_addr.sin_family = AF_INET;
    hp = gethostbyname(argv[1]);
    if (hp == (struct hostent *)0) {
        fprintf(stderr, "Unknown host %s (h_errno=%d)\n", argv[1], h_errno);
        exit(1);
    }
    memcpy((void*)&g_serv_addr.sin_addr, (void*)hp->h_addr, hp->h_length);

    /****************************************************/
    /* Get remote port number from 2nd command-line arg */
    g_serv_addr.sin_port = htons(atoi(argv[2]));

    /****************************************************/
    /* Encode and send some frames                       */
    EncodeDummyRLCPDU1();
    SendFrame(RLC_TM_MODE,
              DIRECTION_DOWNLINK,
              0   /* Priority */,
              101 /* UEId */,
              CHANNEL_TYPE_CCCH,
              0   /* Channel Id */,
              0   /* UMSequenceNumberLength */);

    EncodeDummyRLCPDU2();
    SendFrame(RLC_AM_MODE,
              DIRECTION_UPLINK,
              1   /* Priority */,
              101 /* UEId */,
              CHANNEL_TYPE_DRB,
              3   /* Channel Id */,
              0   /* UMSequenceNumberLength */);

    EncodeDummyRLCPDU3();
    SendFrame(RLC_AM_MODE,
              DIRECTION_UPLINK,
              1   /* Priority */,
              101 /* UEId */,
              CHANNEL_TYPE_DRB,
              3   /* Channel Id */,
              0   /* UMSequenceNumberLength */);


    /* Close local socket */
    close(g_sockfd);

    return EXIT_SUCCESS;

}
