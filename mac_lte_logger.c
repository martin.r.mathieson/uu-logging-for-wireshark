/* mac_lte_logger.c
 *
 * Example code for sending MAC LTE frames over UDP
 * Written by Martin Mathieson, with input from Kiran Kumar
 * This header file may also be distributed under
 * the terms of the BSD Licence as follows:
 * 
 * Copyright (C) 2009 Martin Mathieson. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE
 */


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

typedef unsigned char  guint8;
typedef unsigned short guint16;
typedef unsigned int   guint32;
typedef unsigned long  guint64;
typedef unsigned int   guint;
typedef int            gint;
typedef int gboolean;

typedef struct {
        time_t  secs;
        int     nsecs;
} nstime_t;


/* Forward-declare types */
struct packet_info;
typedef struct packet_info packet_info;

struct tvbuff_t;
typedef struct tvbuff_t tvbuff_t;

struct proto_tree;
typedef struct proto_tree proto_tree;

#include "../wireshark/epan/dissectors/packet-mac-lte.h"

/* TODO: add an example of sending PHY info */


/* Globals where each frame is composed before sending */
static unsigned char g_PDUBuffer[16000];
static unsigned int  g_PDUOffset;
static unsigned char g_frameBuffer[16000];
static unsigned int  g_frameOffset;

/* UDP socket used for sending frames */
static int                  g_sockfd;

/* Remote serveraddress (where Wireshark is running) */
static struct sockaddr_in   g_serv_addr;


/* Write an RAR PDU */
static void EncodeDummyMACPDU1(void)
{
    g_PDUOffset = 0;

    /* RAR */
    g_PDUBuffer[g_PDUOffset++] = 0x40;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0xdc;
    g_PDUBuffer[g_PDUOffset++] = 0x0c;
    g_PDUBuffer[g_PDUOffset++] = 0x21;
    g_PDUBuffer[g_PDUOffset++] = 0x21;
}

/* Write an uplink PDU (with dummy CCCH payload)*/
static void EncodeDummyMACPDU2(void)
{
    g_PDUOffset = 0;

    /* RAR */
    g_PDUBuffer[g_PDUOffset++] = 0x3f;
    g_PDUBuffer[g_PDUOffset++] = 0x3f;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0xa5;
    g_PDUBuffer[g_PDUOffset++] = 0xa5;
    g_PDUBuffer[g_PDUOffset++] = 0xa5;
    g_PDUBuffer[g_PDUOffset++] = 0x79;
    g_PDUBuffer[g_PDUOffset++] = 0x79;
    g_PDUBuffer[g_PDUOffset++] = 0x41;
    g_PDUBuffer[g_PDUOffset++] = 0x41;
    g_PDUBuffer[g_PDUOffset++] = 0x36;
    g_PDUBuffer[g_PDUOffset++] = 0x36;
    g_PDUBuffer[g_PDUOffset++] = 0x36;
}

/* Write a (dummy) BCH PDU */
static void EncodeDummyMACPDU3(void)
{
    g_PDUOffset = 0;

    /* BCH (nonsense data) */
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
}




/*******************************************/
/* Add framing header to MAC PDU and send. */
void SendFrame(guint8 radioType, guint8 direction, guint8 rntiType,
               guint16 rnti, guint16 ueid, guint16 subframeNumber,
               guint8 isPredefinedData, guint8 retx, guint8 crcStatus)
{
    ssize_t bytesSent;
    g_frameOffset = 0;
    unsigned short tmp16;

    /********************************************************************/
    /* Fixed start to each frame (allowing heuristic dissector to work) */
    /* Not NULL terminated */
    memcpy(g_frameBuffer+g_frameOffset, MAC_LTE_START_STRING,
           strlen(MAC_LTE_START_STRING));
    g_frameOffset += strlen(MAC_LTE_START_STRING);

    /******************************************************************************/
    /* Now write out fixed fields (the mandatory elements of struct mac_lte_info) */
    g_frameBuffer[g_frameOffset++] = radioType;
    g_frameBuffer[g_frameOffset++] = direction;
    g_frameBuffer[g_frameOffset++] = rntiType;

    /*************************************/
    /* Now optional fields               */

    /* RNTI */
    g_frameBuffer[g_frameOffset++] = MAC_LTE_RNTI_TAG;
    tmp16 = htons(rnti);
    memcpy(g_frameBuffer+g_frameOffset, &tmp16, 2);
    g_frameOffset += 2;

    /* UEId */
    g_frameBuffer[g_frameOffset++] = MAC_LTE_UEID_TAG;
    tmp16 = htons(ueid);
    memcpy(g_frameBuffer+g_frameOffset, &tmp16, 2);
    g_frameOffset += 2;

    /* Subframe number */
    g_frameBuffer[g_frameOffset++] = MAC_LTE_FRAME_SUBFRAME_TAG;
    tmp16 = htons(ueid);
    memcpy(g_frameBuffer+g_frameOffset, &tmp16, 2);
    g_frameOffset += 2;

    g_frameBuffer[g_frameOffset++] = MAC_LTE_CRC_STATUS_TAG;
    g_frameBuffer[g_frameOffset++] = crcStatus;


    /***********************************************************/
    /* For these optional fields, no need to encode if value is default */
    if (!isPredefinedData) {
        g_frameBuffer[g_frameOffset++] = MAC_LTE_PREDEFINED_DATA_TAG;
        g_frameBuffer[g_frameOffset++] = isPredefinedData;
    }

    if (retx != 0) {
        g_frameBuffer[g_frameOffset++] = MAC_LTE_RETX_TAG;
        g_frameBuffer[g_frameOffset++] = retx;
    }


    /***************************************/
    /* Now write the MAC PDU               */
    g_frameBuffer[g_frameOffset++] = MAC_LTE_PAYLOAD_TAG;

    /* Append actual PDU  */
    memcpy(g_frameBuffer+g_frameOffset, g_PDUBuffer, g_PDUOffset);
    g_frameOffset += g_PDUOffset;

    /* Send out the data over the UDP socket */
    bytesSent = sendto(g_sockfd, g_frameBuffer, g_frameOffset, 0,
                      (const struct sockaddr*)&g_serv_addr, sizeof(g_serv_addr));
    if (bytesSent != g_frameOffset) {
        fprintf(stderr, "sendto() failed - expected %u bytes, got %lu (errno=%d)\n",
                g_frameOffset, bytesSent, errno);
        exit(1);
    }
}


/*************************************************************************/
/* Main function                                                         */
/*  - set up socket + aserver address                                    */
/*  - encode and send some example LTE MAC frames using framing protocol */
int main(int argc, char *argv[])
{
    struct hostent *hp;

    if (argc < 3) {
        fprintf(stderr, "Usage: coclient <server-host> <server-port>\n");
        exit(1);
    }

    /***********************************/
    /* Create local socket             */
    g_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (g_sockfd == -1) {
        fprintf(stderr, "Error trying to create socket (errno=%d)\n", errno);
        exit(1);
    }

    /***************************************************/
    /* Get remote IP address from 1st command-line arg */
    g_serv_addr.sin_family = AF_INET;
    hp = gethostbyname(argv[1]);
    if (hp == (struct hostent *)0) {
        fprintf(stderr, "Unknown host %s (h_errno=%d)\n", argv[1], h_errno);
        exit(1);
    }
    memcpy((void*)&g_serv_addr.sin_addr, (void*)hp->h_addr, hp->h_length);

    /****************************************************/
    /* Get remote port number from 2nd command-line arg */
    g_serv_addr.sin_port = htons(atoi(argv[2]));

    /****************************************************/
    /* Encode and send some frames                       */
    EncodeDummyMACPDU1();
    SendFrame(FDD_RADIO, DIRECTION_DOWNLINK, RA_RNTI,
              1   /* RNTI */,
              101 /* UEId */,
              1   /* Subframe number */,
              0   /* isPredefined */,
              0   /* retx */,
              1   /* CRCStatus (i.e. OK) */);

    EncodeDummyMACPDU2();
    SendFrame(FDD_RADIO, DIRECTION_UPLINK, C_RNTI,
              50   /* RNTI */,
              101  /* UEId */,
              2    /* Subframe number */,
              0    /* isPredefined */,
              1    /* retx */,
              1    /* CRCStatus (i.e. OK) */);

    EncodeDummyMACPDU3();
    SendFrame(FDD_RADIO, DIRECTION_DOWNLINK, NO_RNTI,
              0    /* RNTI */,
              0    /* UEId */,
              3    /* Subframe number */,
              0    /* isPredefined */,
              0    /* retx */,
              0    /* CRCStatu (i.e. CRC error) */);


    /* Close local socket */
    close(g_sockfd);

    return EXIT_SUCCESS;
}
