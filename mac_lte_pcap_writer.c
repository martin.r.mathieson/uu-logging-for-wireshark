/* mac_lte_pcap_writer.c
 *
 * Sample code for writing MAC PDUs into PCAP format, for use with
 * Wireshark's mac-lte-framed dissector
 *
 * Copyright (C) 2012 Martin Mathieson. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE
 */

#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>

/* This is the first of the 'unused' DLTs.  A proper encapsulation was never
 * proposed to tcpdump.org..
/*
#define MAC_LTE_DLT 147


/********************************************************/
/* Definitions and descriptions come from:
    https://gitlab.com/wireshark/wireshark/~/wikis/Development/LibpcapFileFormat */

/* This structure gets written to the start of the file */
typedef struct pcap_hdr_s {
        unsigned int   magic_number;   /* magic number */
        unsigned short version_major;  /* major version number */
        unsigned short version_minor;  /* minor version number */
        unsigned int   thiszone;       /* GMT to local correction */
        unsigned int   sigfigs;        /* accuracy of timestamps */
        unsigned int   snaplen;        /* max length of captured packets, in octets */
        unsigned int   network;        /* data link type */
} pcap_hdr_t;

/* This structure precedes each packet */
typedef struct pcaprec_hdr_s {
        unsigned int   ts_sec;         /* timestamp seconds */
        unsigned int   ts_usec;        /* timestamp microseconds */
        unsigned int   incl_len;       /* number of octets of packet saved in file */
        unsigned int   orig_len;       /* actual length of packet */
} pcaprec_hdr_t;



/* Glib types */
typedef unsigned char  guint8;
typedef unsigned short guint16;
typedef unsigned int   guint32;
typedef unsigned long  guint64;
typedef unsigned int   guint;
typedef int            gint;
typedef int gboolean;

typedef struct {
        time_t  secs;
        int     nsecs;
} nstime_t;


/* Forward-declare types */
struct packet_info;
typedef struct packet_info packet_info;

struct tvbuff_t;
typedef struct tvbuff_t tvbuff_t;

struct proto_tree;
typedef struct proto_tree proto_tree;

#include "../wireshark/epan/dissectors/packet-mac-lte.h"


/**************************************************************************/
/* API functions for opening/writing/closing MAC-LTE PCAP files           */

/* Open the file and write file header */
FILE *MAC_LTE_PCAP_Open(const char *fileName)
{
    pcap_hdr_t file_header =
    {
        0xa1b2c3d4,   /* magic number */
        2, 4,         /* version number is 2.4 */
        0,            /* timezone */
        0,            /* sigfigs - apparently all tools do this */
        65535,        /* snaplen - this should be long enough */
        MAC_LTE_DLT   /* Data Link Type (DLT).  Set as unused value 147 for now */
    };

    FILE *fd = fopen(fileName, "w");
    if (fd == NULL) {
        printf("Failed to open file \"%s\" for writing\n", fileName);
        return NULL;
    }

    /* Write the file header */
    fwrite(&file_header, sizeof(pcap_hdr_t), 1, fd);

    return fd;
}

/* Write an individual PDU (PCAP packet header + mac-context + mac-pdu) */
int MAC_LTE_PCAP_WritePDU(FILE *fd, mac_lte_info *context,
                          const unsigned char *PDU, unsigned int length,
                          int subframesSinceCaptureStart)
{
    pcaprec_hdr_t packet_header;
    char context_header[256];
    int offset = 0;
    unsigned short tmp16;

    /* Can't write if file wasn't successfully opened */
    if (fd == NULL) {
        printf("Error: Can't write to empty file handle\n");
        return 0;
    }

    /*****************************************************************/
    /* Context information (same as written by UDP heuristic clients */
    /* See dissect_mac_lte_context_fields() in packet-mac-lte.c for  */
    /* how it is parsed                                              */

    context_header[offset++] = context->radioType;
    context_header[offset++] = context->direction;
    context_header[offset++] = context->rntiType;

    /* RNTI */
    context_header[offset++] = MAC_LTE_RNTI_TAG;
    tmp16 = htons(context->rnti);
    memcpy(context_header+offset, &tmp16, 2);
    offset += 2;

    /* UEId */
    context_header[offset++] = MAC_LTE_UEID_TAG;
    tmp16 = htons(context->ueid);
    memcpy(context_header+offset, &tmp16, 2);
    offset += 2;

    /* Subframe number */
    context_header[offset++] = MAC_LTE_FRAME_SUBFRAME_TAG;
    tmp16 = (context->sysframeNumber << 4) + context->subframeNumber;
    tmp16 = htons(tmp16);
    memcpy(context_header+offset, &tmp16, 2);
    offset += 2;

    /* CRC Status */
    context_header[offset++] = MAC_LTE_CRC_STATUS_TAG;
    context_header[offset++] = crc_success;

    /* Data tag immediately preceding PDU */
    context_header[offset++] = MAC_LTE_PAYLOAD_TAG;


    /****************************************************************/
    /* PCAP Header                                                  */
    /* TODO: Timestamp might want to be relative to a more sensible
       base time... */
    packet_header.ts_sec = subframesSinceCaptureStart / 1000;
    packet_header.ts_usec = (subframesSinceCaptureStart % 1000) * 1000;
    packet_header.incl_len = offset + length;
    packet_header.orig_len = offset + length;

    /***************************************************************/
    /* Now write everything to the file                            */
    fwrite(&packet_header, sizeof(pcaprec_hdr_t), 1, fd);
    /* Context bytes (set above) */
    fwrite(context_header, 1, offset, fd);
    fwrite(PDU, 1, length, fd);

    return 1;
}

/* Close the PCAP file */
void MAC_LTE_PCAP_Close(FILE *fd)
{
    fclose(fd);
}



/**********************************************************/
/* Example LTE MAC PDUs that will be written in this test */

/* An RAR PDU */
const unsigned char PDU1[] = { 0x40, 0x00, 0x00, 0xdc, 0x0c, 0x21, 0x21 };
unsigned int        PDU1_length = 7;
mac_lte_info  PDU1_context =
{
    FDD_RADIO, DIRECTION_DOWNLINK, RA_RNTI,
    3,        /* RNTI */
    101,      /* UEId */
    1,        /* Sysframe number */
    2         /* Subframe number */
};


/* An ULSCH PDU */
const unsigned char PDU2[] = { 0x3f, 0x3f, 0x00, 0x60, 0xa5, 0xa5, 0x79, 0x79, 0x41 };
unsigned int        PDU2_length = 9;
mac_lte_info  PDU2_context =
{
    FDD_RADIO, DIRECTION_UPLINK, C_RNTI,
    50,       /* RNTI */
    102,      /* UEId */
    1,        /* Sysframe number */
    4,        /* Subframe number */
};

/* A DLSCH PDU (BCH-BCH) */
const unsigned char PDU3[] = { 0x01, 0x02, 0x03 };
unsigned int        PDU3_length = 3;
mac_lte_info  PDU3_context =
{
    FDD_RADIO, DIRECTION_DOWNLINK, NO_RNTI,
    0,       /* RNTI */
    0,       /* UEId */
    1,       /* Sysframe number */
    5        /* Subframe number */
};



/*************************************************************/
/* Opens a file and write some PDUs into it                  */
int main(int argc, char **argv)
{
    /* Create log file */
    FILE *fd;
    printf("Running mac_pcap_sample_code: writing to \"mac_lte_dlt_147.pcap\"...\n");
    fd = MAC_LTE_PCAP_Open("mac_lte_dlt_147.pcap");

    if (fd != NULL) {
        /* Now add some PDUs */
        MAC_LTE_PCAP_WritePDU(fd, &PDU1_context, PDU1, PDU1_length, 1001);
        MAC_LTE_PCAP_WritePDU(fd, &PDU2_context, PDU2, PDU2_length, 2002);
        MAC_LTE_PCAP_WritePDU(fd, &PDU3_context, PDU3, PDU3_length, 3003);

        MAC_LTE_PCAP_Close(fd);

        printf("....  Done\n");
    }

    return 0;
}
