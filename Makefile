all: mac_lte_logger mac_lte_pcap_writer rlc_lte_logger pdcp_lte_logger

WIRESHARK_FOLDER = ~/wireshark
INCLUDES = -I ${WIRESHARK_FOLDER} -I ${WIRESHARK_FOLDER}/include
CFLAGS += ${INCLUDES} -Wall -Werror -Wshadow

mac_lte_logger: mac_lte_logger.c
	${CC} ${CFLAGS} $< -o $@

mac_lte_pcap_writer: mac_lte_pcap_writer.c
	${CC} ${CFLAGS} $< -o $@

rlc_lte_logger: rlc_lte_logger.c
	${CC} ${CFLAGS} $< -o $@

pdcp_lte_logger: pdcp_lte_logger.c
	${CC} ${CFLAGS} $< -o $@

clean:
	rm -rf mac_lte_logger mac_lte_pcap_writer rlc_lte_logger pdcp_lte_logger *.pcap __pycache__

