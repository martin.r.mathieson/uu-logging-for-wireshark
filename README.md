# Uu Logging for Wireshark

This repo is a place to store sample programs that may be used to create capture files that may be read by
[Wireshark](https://www.wireshark.org) containing Uu frames for
[LTE](https://gitlab.com/wireshark/wireshark/-/wikis/LTEProtocolFamily) and (perhaps later) NR.

Please see the Wireshark wiki's protocol pages for [MAC-LTE](https://gitlab.com/wireshark/wireshark/-/wikis/MAC-LTE),
[RLC-LTE](https://gitlab.com/wireshark/wireshark/-/wikis/RLC-LTE) and
[PDCP-LTE](https://gitlab.com/wireshark/wireshark/-/wikis/PDCP-LTE), which now refer to the programs in this project.

These programs are really only to show what is involved in logging these types of frames.  They are written in C using the BSD license, so feel free to use as is convenient.  A simple Makefile and and pytest file
are also here for my convenience.

If there is value in also creating the NR equivalents, do let me know.

# mac_lte_pcap_writer.c
[Shows](https://gitlab.com/martin.r.mathieson/uu-logging-for-wireshark/-/blob/master/mac_lte_pcap_writer.c) how to write frames directly to a pcap file in a format that the **mac-lte-framed** dissector may read.

# mac_lte_logger.c
[Shows](https://gitlab.com/martin.r.mathieson/uu-logging-for-wireshark/-/blob/master/mac_lte_logger.c) an example of sending frames over UDP, that when captured may be decoded using **mac-lte-udp**

# rlc_lte_logger.c
[Shows](https://gitlab.com/martin.r.mathieson/uu-logging-for-wireshark/-/blob/master/rlc_lte_logger.c) an example of sending frames over UDP, that when captured may be decoded using **rlc-lte-udp**

# pdcp_lte_logger.c
[Shows](https://gitlab.com/martin.r.mathieson/uu-logging-for-wireshark/-/blob/master/rlc_lte_logger.c) an example of sending frames over UDP, that when captured may be decoded using **pdcp-lte-udp**

# test_progs.py
[This file](https://gitlab.com/martin.r.mathieson/uu-logging-for-wireshark/-/blob/master/test_progs.py), which may be run under **pytest**, will run each of the build programs and do a simple check that the PDUs written (or captured) can be decoded.