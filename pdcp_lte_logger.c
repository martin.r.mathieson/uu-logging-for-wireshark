/* pdcp_lte_logger.c
 *
 * Example code for sending PDCP LTE frames over UDP
 * Written by Pascal Quantin and Martin Mathieson
 * This header file may also be distributed under
 * the terms of the BSD Licence as follows:
 * 
 * Copyright (C) 2009 Pascal Quantin & Martin Mathieson. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE
 */



#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

typedef unsigned char  guint8;
typedef unsigned short guint16;
typedef unsigned int   guint32;
typedef int            gint;
typedef int            gboolean;
#ifndef FALSE
#define	FALSE (0)
#endif
#ifndef TRUE
#define TRUE  (!FALSE)
#endif

/* Forward-declare types */
struct packet_info;
typedef struct packet_info packet_info;
struct tvbuff_t;
typedef struct tvbuff_t tvbuff_t;
struct proto_item;
typedef struct proto_item proto_item;


#include "../wireshark/epan/dissectors/packet-pdcp-lte.h"


/* Globals where each frame is composed before sending */
static unsigned char g_PDUBuffer[16000];
static unsigned int  g_PDUOffset;
static unsigned char g_frameBuffer[16000];
static unsigned int  g_frameOffset;

/* UDP socket used for sending frames */
static int                  g_sockfd;

/* Remote serveraddress (where Wireshark is running) */
static struct sockaddr_in   g_serv_addr;


/* Write a Signaling Plane PDCP PDU */
static void EncodeDummyPDCPPDU1(void)
{
    g_PDUOffset = 0;

    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x48;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0xE0;
    g_PDUBuffer[g_PDUOffset++] = 0xE8;
    g_PDUBuffer[g_PDUOffset++] = 0x60;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x6A;
    g_PDUBuffer[g_PDUOffset++] = 0x40;
    g_PDUBuffer[g_PDUOffset++] = 0x18;
    g_PDUBuffer[g_PDUOffset++] = 0x40;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
}

/* Write a User Plane Control PDCP PDU */
static void EncodeDummyPDCPPDU2(void)
{
    g_PDUOffset = 0;

    g_PDUBuffer[g_PDUOffset++] = 0x03;
    g_PDUBuffer[g_PDUOffset++] = 0x10;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0xDD;
    g_PDUBuffer[g_PDUOffset++] = 0xBA;
}

/* Write a Data Plane Data PDCP PDU */
static void EncodeDummyPDCPPDU3(void)
{
    g_PDUOffset = 0;

    g_PDUBuffer[g_PDUOffset++] = 0x81;
    g_PDUBuffer[g_PDUOffset++] = 0x09;
    g_PDUBuffer[g_PDUOffset++] = 0x45;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x21;
    g_PDUBuffer[g_PDUOffset++] = 0xF6;
    g_PDUBuffer[g_PDUOffset++] = 0xAD;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x80;
    g_PDUBuffer[g_PDUOffset++] = 0x11;
    g_PDUBuffer[g_PDUOffset++] = 0xC2;
    g_PDUBuffer[g_PDUOffset++] = 0xCA;
    g_PDUBuffer[g_PDUOffset++] = 0xC0;
    g_PDUBuffer[g_PDUOffset++] = 0xA8;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x01;
    g_PDUBuffer[g_PDUOffset++] = 0xC0;
    g_PDUBuffer[g_PDUOffset++] = 0xA8;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x02;
    g_PDUBuffer[g_PDUOffset++] = 0x05;
    g_PDUBuffer[g_PDUOffset++] = 0x74;
    g_PDUBuffer[g_PDUOffset++] = 0x04;
    g_PDUBuffer[g_PDUOffset++] = 0xD2;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x0D;
    g_PDUBuffer[g_PDUOffset++] = 0xD6;
    g_PDUBuffer[g_PDUOffset++] = 0x1C;
    g_PDUBuffer[g_PDUOffset++] = 0x47;
    g_PDUBuffer[g_PDUOffset++] = 0x00;
    g_PDUBuffer[g_PDUOffset++] = 0x45;
    g_PDUBuffer[g_PDUOffset++] = 0x1D;
    g_PDUBuffer[g_PDUOffset++] = 0x12;
}

/*******************************************/
/* Add framing header to PDCP PDU and send. */
void SendFrame(guint8 direction, LogicalChannelType channelType,
               BCCHTransportType BCCHTransport, gboolean no_header_pdu,
               enum pdcp_plane plane, guint8 seqnum_length, gboolean rohc_compression,
               unsigned short rohc_ip_version, gboolean cid_inclusion_info,
               gboolean large_cid_present, enum rohc_mode mode,
               gboolean rnd, gboolean udp_checkum_present, unsigned short profile)
{
    ssize_t bytesSent;
    g_frameOffset = 0;
    unsigned short tmp16;

    /********************************************************************/
    /* Fixed start to each frame (allowing heuristic dissector to work) */
    /* Not NULL terminated */
    memcpy(g_frameBuffer+g_frameOffset, PDCP_LTE_START_STRING,
           strlen(PDCP_LTE_START_STRING));
    g_frameOffset += strlen(PDCP_LTE_START_STRING);

    /*******************************************************************************/
    /* Now write out fixed fields (the mandatory elements of struct pdcp_lte_info) */
    g_frameBuffer[g_frameOffset++] = no_header_pdu;
    g_frameBuffer[g_frameOffset++] = plane;
    g_frameBuffer[g_frameOffset++] = rohc_compression;

    /*************************************/
    /* Now conditional fields            */
    
    /* Seq Num Length */
    if (plane == USER_PLANE) {
        g_frameBuffer[g_frameOffset++] = PDCP_LTE_SEQNUM_LENGTH_TAG;
        g_frameBuffer[g_frameOffset++] = seqnum_length;
    }

    /*************************************/
    /* Now optional fields               */

    /* Direction */
    g_frameBuffer[g_frameOffset++] = PDCP_LTE_DIRECTION_TAG;
    g_frameBuffer[g_frameOffset++] = direction;

    /* Logical Channel Type */
    g_frameBuffer[g_frameOffset++] = PDCP_LTE_LOG_CHAN_TYPE_TAG;
    g_frameBuffer[g_frameOffset++] = channelType;

    /* BCCH Transport Type */
    g_frameBuffer[g_frameOffset++] = PDCP_LTE_BCCH_TRANSPORT_TYPE_TAG;
    g_frameBuffer[g_frameOffset++] = BCCHTransport;

    /* RoHC IP Version */
    g_frameBuffer[g_frameOffset++] = PDCP_LTE_ROHC_IP_VERSION_TAG;
    tmp16 = htons(rohc_ip_version);
    memcpy(g_frameBuffer+g_frameOffset, &tmp16, 2);
    g_frameOffset += 2;

    /* RoHC Cid Inclusion Info */
    g_frameBuffer[g_frameOffset++] = PDCP_LTE_ROHC_CID_INC_INFO_TAG;
    g_frameBuffer[g_frameOffset++] = cid_inclusion_info;

    /* RoHC Large Cid Present */
    g_frameBuffer[g_frameOffset++] = PDCP_LTE_ROHC_LARGE_CID_PRES_TAG;
    g_frameBuffer[g_frameOffset++] = large_cid_present;

    /* RoHC Mode */
    g_frameBuffer[g_frameOffset++] = PDCP_LTE_ROHC_MODE_TAG;
    g_frameBuffer[g_frameOffset++] = mode;

    /* RoHC Rand */
    g_frameBuffer[g_frameOffset++] = PDCP_LTE_ROHC_RND_TAG;
    g_frameBuffer[g_frameOffset++] = rnd;

    /* RoHC UDP Checksum Present */
    g_frameBuffer[g_frameOffset++] = PDCP_LTE_ROHC_UDP_CHECKSUM_PRES_TAG;
    g_frameBuffer[g_frameOffset++] = udp_checkum_present;

    /* RoHC Profile */
    g_frameBuffer[g_frameOffset++] = PDCP_LTE_ROHC_PROFILE_TAG;
    tmp16 = htons(profile);
    memcpy(g_frameBuffer+g_frameOffset, &tmp16, 2);
    g_frameOffset += 2;

    /***************************************/
    /* Now write the PDCP PDU              */
    g_frameBuffer[g_frameOffset++] = PDCP_LTE_PAYLOAD_TAG;

    /* Append actual PDU  */
    memcpy(g_frameBuffer+g_frameOffset, g_PDUBuffer, g_PDUOffset);
    g_frameOffset += g_PDUOffset;

    /* Send out the data over the UDP socket */
    bytesSent = sendto(g_sockfd, g_frameBuffer, g_frameOffset, 0,
                      (const struct sockaddr*)&g_serv_addr, sizeof(g_serv_addr));
    if (bytesSent != g_frameOffset) {
        fprintf(stderr, "sendto() failed - expected %u bytes, got %lu (errno=%d)\n",
                g_frameOffset, bytesSent, errno);
        exit(1);
    }
}


/**************************************************************************/
/* Main function                                                          */
/*  - set up socket + aserver address                                     */
/*  - encode and send some example LTE PDCP frames using framing protocol */
int main(int argc, char *argv[])
{
    struct hostent *hp;

    if (argc < 3) {
        fprintf(stderr, "Usage: coclient <server-host> <server-port>\n");
        exit(1);
    }

    /***********************************/
    /* Create local socket             */
    g_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (g_sockfd == -1) {
        fprintf(stderr, "Error trying to create socket (errno=%d)\n", errno);
        exit(1);
    }

    /***************************************************/
    /* Get remote IP address from 1st command-line arg */
    g_serv_addr.sin_family = AF_INET;
    hp = gethostbyname(argv[1]);
    if (hp == (struct hostent *)0) {
        fprintf(stderr, "Unknown host %s (h_errno=%d)\n", argv[1], h_errno);
        exit(1);
    }
    memcpy((void*)&g_serv_addr.sin_addr, (void*)hp->h_addr, hp->h_length);

    /****************************************************/
    /* Get remote port number from 2nd command-line arg */
    g_serv_addr.sin_port = htons(atoi(argv[2]));

    /****************************************************/
    /* Encode and send some frames                       */
    EncodeDummyPDCPPDU1();
    SendFrame(DIRECTION_UPLINK,
              Channel_DCCH,
              0     /* BCCHTransport */,
              FALSE /* no_header_pdu */,
              SIGNALING_PLANE,
              0     /* seqnum_length */,
              FALSE /* rohc_compression */,
              0     /* rohc_ip_version */,
              FALSE /* cid_inclusion_info */,
              FALSE /* large_cid_present */,
              UNIDIRECTIONAL,
              FALSE /* rnd */,
              FALSE /* udp_checkum_present */,
              0     /* profile */);

    EncodeDummyPDCPPDU2();
    SendFrame(DIRECTION_UPLINK,
              Channel_DCCH,
              0     /* BCCHTransport */,
              FALSE /* no_header_pdu */,
              USER_PLANE,
              PDCP_SN_LENGTH_12_BITS,
              FALSE /* rohc_compression */,
              0     /* rohc_ip_version */,
              FALSE /* cid_inclusion_info */,
              FALSE /* large_cid_present */,
              UNIDIRECTIONAL,
              FALSE /* rnd */,
              FALSE /* udp_checkum_present */,
              0     /* profile */);

    EncodeDummyPDCPPDU3();
    SendFrame(DIRECTION_DOWNLINK,
              Channel_DCCH,
              0     /* BCCHTransport */,
              FALSE /* no_header_pdu */,
              USER_PLANE,
              PDCP_SN_LENGTH_12_BITS,
              FALSE /* rohc_compression */,
              0     /* rohc_ip_version */,
              FALSE /* cid_inclusion_info */,
              FALSE /* large_cid_present */,
              UNIDIRECTIONAL,
              FALSE /* rnd */,
              FALSE /* udp_checkum_present */,
              0     /* profile */);


    /* Close local socket */
    close(g_sockfd);

    return EXIT_SUCCESS;

}
