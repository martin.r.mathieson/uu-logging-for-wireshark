#!/usr/bin/env python3

import pytest
import os
import time
import subprocess
import signal

tcpdump_process = None

def start_capture(filename):
    global tcpdump_process
    command = 'tcpdump -i lo -s 0 -w ' + filename + ' not icmp'
    tcpdump_process = subprocess.Popen(command,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE,
                                       shell=True,
                                       preexec_fn=os.setsid)
    time.sleep(0.2)

def stop_capture():
    time.sleep(1.5)
    global tcpdump_process
    os.killpg(tcpdump_process.pid, signal.SIGINT)

# TODO: add extra args to use to override preferences to force decode?
def check_frames_in_capture(pcapfile, filter, num_frames):
    # Run through tshark and check expected number of matching frames
    tshark_command = [ 'tshark', '-Y', filter, '-r', pcapfile ]
    output = subprocess.check_output(tshark_command)
    lines = output.splitlines()
    assert len(lines) == num_frames





def test_mac_lte_pcap_writer():
    # Run prog
    write_command = [ './mac_lte_pcap_writer' ]
    ret = subprocess.call(write_command)
    assert ret==0
    # Check file exists and has expected frames
    assert os.path.exists("mac_lte_dlt_147.pcap")
    # TODO: run with overriden user_dlts?
    check_frames_in_capture("mac_lte_dlt_147.pcap", "mac-lte", 3)

def test_mac_lte_logger():
    start_capture('mac_lte_log.pcap')

    # Run logger
    send_command = [ './mac_lte_logger', '127.0.0.1', '6000' ]
    ret = subprocess.call(send_command)
    assert ret==0

    # Ensure that we got frames
    stop_capture()
    assert os.path.exists('mac_lte_log.pcap')
    check_frames_in_capture("mac_lte_log.pcap", "mac-lte", 3)

def test_rlc_lte_logger():
    start_capture('rlc_lte_log.pcap')

    # Run logger
    send_command = [ './rlc_lte_logger', '127.0.0.1', '6000' ]
    ret = subprocess.call(send_command)
    assert ret==0

    # Ensure that we got frames
    stop_capture()
    assert os.path.exists('rlc_lte_log.pcap')
    check_frames_in_capture("rlc_lte_log.pcap", "rlc-lte", 3)

def test_pdcp_lte_logger():
    start_capture('pdcp_lte_log.pcap')

    # Run logger
    send_command = [ './pdcp_lte_logger', '127.0.0.1', '6000' ]
    ret = subprocess.call(send_command)
    assert ret==0

    # Ensure that we got frames
    stop_capture()
    assert os.path.exists('pdcp_lte_log.pcap')
    check_frames_in_capture("pdcp_lte_log.pcap", "pdcp-lte", 3)
